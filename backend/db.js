const mongoose = require('mongoose');

try {
    mongoose.connect('mongodb://localhost:27017/cashFlow', { useNewUrlParser: true });
    console.log('db connected')
} catch (err) { 
    console.log(err)
 }
