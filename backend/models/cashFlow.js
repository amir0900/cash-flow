const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cashFlowSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    amount: Number,
    name:String,
    type:String
})
 const CashFlow = mongoose.model('CashFlow', cashFlowSchema);

module.exports = CashFlow;