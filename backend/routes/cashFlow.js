const express = require('express');
const router = require('express-promise-router')();
const checkToken = require('../helpers/check-auth')
const cashFlowController = require('../controllers/cashFlow');

router.route('/get')
    .get(checkToken, cashFlowController.get)

router.route('/add')
    .post(checkToken, cashFlowController.add)

router.route('/remove')
    .post(checkToken, cashFlowController.remove)


module.exports = router;