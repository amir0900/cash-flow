const express = require('express');
const router = require('express-promise-router')();

const UserController = require('../controllers/users')
const passport = require('passport');

// get local passport strategy
require('../helpers/router.helpers')

router.route('/signup')
    .post(UserController.signUp)

router.route('/signin')
    .post(passport.authenticate('local', { session: false }), UserController.signIn)


module.exports = router;