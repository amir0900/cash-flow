const User = require('../models/user')
const mongoose = require('mongoose');
const JWT = require('jsonwebtoken');
const { JWT_SECRET } = require('../configuration');

signToken = user => {
    return JWT.sign({
        iss: 'amir',
        sub: user._id,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() + 1) // current time + 1 day
    }, JWT_SECRET);
}

module.exports = {
    signUp: async (req, res, next) => {
        // expect user & password
        const { email, password } = req.body;

        // check if there is a user with the same email
        const foundUser = await User.findOne({ email })
        if (foundUser) {
            return res.status(403).json({ error: 'email already in use' })
        }
        // create a new user
        const newUser = new User({ _id: new mongoose.Types.ObjectId(), email, password });
        await newUser.save()

        // Generate a token
        const token = signToken(newUser)
        res.status(200).json({
            token
        })
    },
    signIn: async (req, res, next) => {
        console.log('user sign it')
        // authenticated
        // Generate a token
        const { email, password } = req.body;
        const user = {
            email,
            password
        }
        const token = signToken(user)
        res.status(200).json({
            token
        })
    }
}