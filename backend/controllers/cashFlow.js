const cashFlow = require('../models/cashFlow');
const mongoose = require('mongoose');
const User = require('../models/user');


module.exports = {
    get: async (req, res, next) => {
        // Handle the search and the result
        const user = await User.findById(req.userData.payload.sub)

        // Check if user found
        if (!user) {
            return res.status(500).json({
                message: 'fail to fetch user'
            })
        }
        res.status(200).json({
            data: user.cashFlows
        })
    },
    add: async (req, res, next) => {
        // Store post payload
        const { amount, name, type } = req.body

        // Set a new item schema based
        const item = new cashFlow({
            _id: new mongoose.Types.ObjectId(),
            amount,
            name,
            type
        })

        // Handle the search and the result
        const user = await User.findById(req.userData.payload.sub)
        console.log(user)
        // Check if user found
        if (!user) {
            return res.status(500).json({
                message: 'fail to fetch user'
            })
        }

        // Push the new item from the request
        user.cashFlows.push(item)

        // Save to the DB
        await user.save();

        // Send back the response
        res.status(200).json({
            message: user
        })

    },
    remove: async (req, res, next) => {
        // Get id from the request 
        const { _id } = req.body

        // Handle the search and the result
        const user = await User.findById(req.userData.payload.sub)

        // Check if user found
        if (!user) {
            return res.status(500).json({
                message: 'fail to fetch user'
            })
        }

        // Remove the requested id
        const index = user.cashFlows.filter(object => {
            return object._id != _id;
        })

        // Set the new array 
        user.cashFlows = index

        // Save to the DB
        await user.save();

        // Return the user without the requested id
        res.status(200).json({
            message: user
        })

    }
}