// Web application framework 
const express = require('express');
// Debugger
const morgan = require('morgan');
// Parser middleware for http request
const bodyParser = require("body-parser");
// Prevent cors issue
const cors = require('cors');
// Start the DB
const db = require('./db');

const app = express();


// Middlewares
app.use(morgan('dev'));
app.use(cors())
app.use(bodyParser.json());


//Routes
app.use('/users', require('./routes/users'));
app.use('/cashFlow', require('./routes/cashFlow'));

// Start the server
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`server is listen on port: ${port}`));