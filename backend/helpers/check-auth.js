const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../configuration');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, JWT_SECRET, {complete: true});
        req.userData = decoded;
        next();
    } catch (error) {
        console.log(error)
        return res.status(401).json({
            message: 'Auth failed'
        });
    }
};