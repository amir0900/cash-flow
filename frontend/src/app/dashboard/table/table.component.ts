import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { CashFlowService } from 'src/app/core/services';
import { CashFlow } from '../../core/models';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})


export class TableComponent implements OnInit {
  tableData: CashFlow;
  constructor(private cashFlow: CashFlowService) {}



  ngOnInit() {
    this.cashFlow.getList().subscribe(response => {
      this.tableData = response.data;
      console.log(this.tableData);
    });
  }

}
