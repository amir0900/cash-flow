import { Component, OnInit } from '@angular/core';
import { CashFlowService } from 'src/app/core/services';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-add-cash-flow',
  templateUrl: './add-cash-flow.component.html',
  styleUrls: ['./add-cash-flow.component.scss']
})

export class AddFlowCashComponent implements OnInit {
  source: string[] = ['income', 'outcome'];
  addItemForm: FormGroup;
  constructor(private fb: FormBuilder, private cashFlow: CashFlowService) {
    this.addItemForm = this.fb.group({
      name: ['', Validators.required],
      amount: ['', Validators.required],
      type: ['', Validators.required]
    });
  }
  ngOnInit() {
  }

  addCashFlow(): void {
    const item = this.addItemForm.value;
    this.cashFlow.add(item).subscribe(
      data => {

    },
      err => {
        console.log(err);
      });
  }
}
