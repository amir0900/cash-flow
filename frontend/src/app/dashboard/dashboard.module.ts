import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table/table.component';
import { AddFlowCashComponent } from './add-cash-flow/add-cash-flow.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TableComponent, AddFlowCashComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    TableComponent, AddFlowCashComponent
  ]
})
export class DashboardModule { }
