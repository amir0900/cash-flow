export interface CashFlow {
    id: string;
    name: string;
    amount: number;
    type: string;
}
