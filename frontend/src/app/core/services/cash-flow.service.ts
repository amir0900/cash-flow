import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { CashFlow } from '../models';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CashFlowService {

  constructor(private api: ApiService) { }

  getList(): Observable<CashFlow> {
    return this.api.get('/cashflow/get').pipe(map(
      response => {
        return response;
      }));
  }
  add(item): Observable<CashFlow> {
    return this.api.post('/cashflow/add', item).pipe(map(
      response => {
        return response;
      }));
  }
  remove(id): Observable<string> {
    return this.api.post('/cashflow/remove', id).pipe(map(
      response => {
        return response;
      }));
  }
}

