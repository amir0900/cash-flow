import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../core/services/user.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  authForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService) {
    this.authForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

  }


  ngOnInit() {
  }

  submitForm(type) {
    const user = this.authForm.value;
    this.userService
      .attemptAuth(user, type)
      .subscribe(
        data => {
          this.router.navigateByUrl('/dash-board');
        },
        err => {
          console.log(err);
        }
      );
  }

}
